(require 'cl-lib)

(defun --range-end (end)
  (cl-loop for x below end collect x))

(defun --range-star-end (start end)
  (cl-loop for x from start below end collect x))

(defun --range-start-end-step (start end step)
  (cl-loop for x from start below end by step collect x))

(defun range (&rest args)
  (cl-case (length args)
    (1 (apply #'--range-end args))
    (2 (apply #'--range-star-end args))
    (3 (apply #'--range-start-end-step args))
    (0 (throw 'type-error
              "range expected at least 1 argument, got 0"))
    (otherwise
     (throw 'type-error
            (format-message
             "range expect at most 3 arguments, got %d" (length args))))))
