#!/bin/env bash
pip3 install epc orjson sexpdata six setuptools paramiko rapidfuzz watchdog
git clone git@github.com:manateelazycat/lsp-bridge.git \
    -b master ~/.emacs.d/vendor/lsp-bridge
