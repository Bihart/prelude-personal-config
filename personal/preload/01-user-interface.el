(use-package prelude-custom
  :custom
  (prelude-flyspell nil)
  (prelude-minimalistic-ui t)
  :config
  (scroll-bar-mode -1))
