(require 'prelude-packages)

(prelude-require-packages
 '(ef-themes
   apropospriate-theme
   multiple-cursors
   company-emoji
   poetry
   terraform-mode))

;;; personal-module.el end here
