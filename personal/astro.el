;; web-mode
(use-package web-mode
  :ensure t)

;; astro-mode
(define-derived-mode astro-mode web-mode "astro")
(setq auto-mode-alist (append '(("\\.astro\\'" . astro-mode)) auto-mode-alist))

auto-mode-alist
;; eglot
(use-package eglot
  :ensure t
  :config
  (add-to-list
   'eglot-server-programs
   '(astro-mode  . ("astro-ls" "--stdio"
                    :initializationOptions
                    (:typescript (:tsdk "./node_modules/typescript/lib"))))))
(add-hook 'astro-mode-hook 'eglot-ensure)
