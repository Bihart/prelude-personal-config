(require 'display-line-numbers)


(global-set-key (kbd  "C-c C-t l") 'display-line-numbers-mode)
