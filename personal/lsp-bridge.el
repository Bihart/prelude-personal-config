(prelude-require-packages '(markdown-mode yasnippet))
(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "multimarkdown")
  :bind (:map markdown-mode-map
              ("C-c C-e" . markdown-do)))

(use-package yasnippet
  :config (yas-global-mode 1))

(use-package lsp-bridge
  :custom
  (lsp-bridge-php-lsp-server "phpactor")
  :bind (([remap xref-find-definitions] . lsp-bridge-find-def)
         ([remap xref-go-back] . lsp-bridge-find-def-return)
         ([reamp xref-find-references] . lsp-bridge-find-references))
  :config (global-lsp-bridge-mode))

(use-package xref
  :bind
  (("M-." . xref-find-definitions)))
