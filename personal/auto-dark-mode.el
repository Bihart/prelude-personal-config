(use-package auto-dark
  :ensure t
  :custom
  (auto-dark-themes '((ef-trio-dark) (ef-summer)))
  (auto-dark-polling-interval-seconds 5)
  (auto-dark-allow-osascript nil)
  (auto-dark-allow-powershell nil)
  :init (auto-dark-mode))

(use-package prelude-custom
  :custom
  (prelude-theme (if (auto-dark--is-dark-mode)
                     (auto-dark--dark-themes)
                   (auto-dark--light-themes))))
